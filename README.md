# Containerized Oracle XE 11g with SSH

This repo contains a [Dockerfile](https://www.docker.com/) to create an image with [Oracle Database 11g Express Edition](http://www.oracle.com/technetwork/database/database-technologies/express-edition/overview/index.html) running in [CentOS 6](http://www.centos.org/)


## How to build

`docker build -t spartact/orassh .`

To change the oracle version:

1. Download oracle version from [Oracle](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html) 
1. Replace file in folder /config/rpm
1. Change rpm name in /config/setup.sh

## How to use

Basically `docker run -p 2022:22 -p 1521:1521 -p -d  spartact/orassh` will start new container and bind it's local ports `1521` and `2022` to host's `1521` and `22` respectively.
Read [Docker documentation](http://docs.docker.com/userguide/usingdocker/) for details.

### Create SSH User

        --env "SSH_USER=ansible" \
        --env "SSH_AUTHORIZED_KEYS=`cat ~/.ssh/id_rsa.pub`"

### Create Oracle User

        --env "ORACLE_USER=appuser1" \
        --env "ORACLE_USER_PASSWORD=password123"
        
If ORACLE_USER_PASSWORD is not defined, password will be set to "oracle".

### Oracle Interface

Connect to the database using the following details:

    hostname: localhost
    port: 1521
    sid: XE
    username: system
    password: oracle
    
Or with the user created by providing env parameter within the container start. (i.E. appuser1/password123)

## Workarounds

Due to install problems with oracle within container running on centos6, oracle installation is without checking the pre requirements

    rpm --nopre -ivh /config/rpm/oracle-xe-11.2.0-1.0.x86_64.rpm


## Credits

Based on [madhead/docker-oracle-xe](https://github.com/madhead/docker-oracle-xe)
