#!/bin/bash

# Example how to start container

docker run --name orassh \
-p 8019:8080 -p 2521:1521 -p 2022:22 \
--env "SSH_AUTHORIZED_KEYS=`cat ~/.ssh/id_rsa.pub`" \
--env "SSH_USER=ansible" \
--env "ORACLE_USER=appuser" \
--env "ORACLE_USER_PASSWORD=d0cker" \
-d spartact/orassh
