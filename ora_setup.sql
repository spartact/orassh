-- Remove expire
ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED; 

-- Increase max session/process
alter system set processes=500 scope=spfile;
alter system reset sessions scope=spfile sid='*';
shutdown immediate;
startup;