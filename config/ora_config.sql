-- Create hpsa_role
create role &2;
grant create session to &2;
grant create table to &2;
grant create sequence to &2;
grant create trigger to &2;
grant create view to &2;
commit;

-- Create user and assign hpsa_role
create user &1 identified by &3 default tablespace USERS quota unlimited on USERS;
grant &2 to &1;
commit;