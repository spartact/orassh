# Pre-requirements
mkdir -p /run/lock/subsys

# Yum install
yum install -y libaio bc initscripts net-tools nc \
openssh-server openssh-clients passwd sudo; \
yum clean all

############################################
# Oracle

# Install Oracle XE
rpm --nopre -ivh /config/rpm/oracle-xe-11.2.0-1.0.x86_64.rpm

# Configure instance
mv /config/xe.rsp /config/init.ora /config/initXETemp.ora /u01/app/oracle/product/11.2.0/xe/config/scripts/
chown oracle:dba /u01/app/oracle/product/11.2.0/xe/config/scripts/*.ora \
                 /u01/app/oracle/product/11.2.0/xe/config/scripts/xe.rsp
chmod 755        /u01/app/oracle/product/11.2.0/xe/config/scripts/*.ora \
                 /u01/app/oracle/product/11.2.0/xe/config/scripts/xe.rsp

# export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
# export ORACLE_SID=XE
# export PATH=$ORACLE_HOME/bin:$PATH

echo 'export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe' > /etc/profile.d/oracle.sh
echo 'export ORACLE_SID=XE' >> /etc/profile.d/oracle.sh
echo 'export PATH=$ORACLE_HOME/bin:$PATH' >> /etc/profile.d/oracle.sh
. /etc/profile.d/oracle.sh

/etc/init.d/oracle-xe configure responseFile=/u01/app/oracle/product/11.2.0/xe/config/scripts/xe.rsp

# Disable password expire & Increase max session/process
# sqlplus -s sys/oracle as sysdba << EOF
#     whenever sqlerror exit sql.sqlcode;
#     set echo off
#     set heading off
#     @ora_config_setup.sql
#     exit;
# EOF


#cd /config/; \
#sh ora_config_setup.sh

# RUN /etc/init.d/oracle-xe start; \
#     cd /tmp/; \
#     sh ora_config_setup.sh; \
#     rm -f /tmp/ora_config_setup*; \
#     /etc/init.d/oracle-xe stop



# SSH
mkdir /var/run/sshd; \
sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd; \
echo "PermitRootLogin no" >>/etc/ssh/sshd_config; \
ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''; \
ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key -N ''

echo 'export VISIBLE=now' >> /etc/profile

# Run
cp /config/start.sh /config/ora_config.sql /config/ora_config_setup.sql /

rm -rf /config/

