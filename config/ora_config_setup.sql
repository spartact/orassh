-- Remove expire
ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED; 

-- Increase max session/process
alter system set processes = 260 scope = spfile;
alter system set sessions = 300 scope = spfile;
alter system set transactions = 330 scope = spfile; 


shutdown immediate;
startup;